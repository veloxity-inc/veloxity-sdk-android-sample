
package net.veloxity.sample.app;


import net.veloxity.sdk.DataUsageListener;
import net.veloxity.sdk.FCMApiKeyException;
import net.veloxity.sdk.FCMAppIdException;
import net.veloxity.sdk.FCMProjectIdException;
import net.veloxity.sdk.FCMSenderIdException;
import net.veloxity.sdk.LicenseException;
import net.veloxity.sdk.ServiceStatus;
import net.veloxity.sdk.Veloxity;
import net.veloxity.sdk.VeloxityOptions;


import android.os.Bundle;
import android.preference.Preference;
import android.preference.SwitchPreference;

import androidx.core.app.NavUtils;
import androidx.appcompat.app.ActionBar;

import android.util.Log;
import android.view.MenuItem;

public class SettingsActivity extends AppCompatPreferenceActivity {

    private SwitchPreference veloxity_sdk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupActionBar();

        addPreferencesFromResource(R.xml.pref_general);

        // Service status can be read from sharedPreferences which is saved before in MainActivity
        // or can be gotten from the getServiceStatus() method of Veloxity SDK.
        boolean serviceStatus = Veloxity.getServiceStatus(getApplicationContext()) == ServiceStatus.RUNNING;

        veloxity_sdk = (SwitchPreference) getPreferenceManager().findPreference("veloxity_sdk");
        veloxity_sdk.setChecked(serviceStatus);
        veloxity_sdk.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                if ((Boolean) newValue) {
                    optIn();
                } else {
                    optOut();
                }
                return true;
            }
        });
    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            if (!super.onMenuItemSelected(featureId, item)) {
                NavUtils.navigateUpFromSameTask(this);
            }
            return true;
        }
        return super.onMenuItemSelected(featureId, item);
    }

    private void optOut() {
        Veloxity.optOut(getApplicationContext());
    }

    private void optIn() {
        try {
            VeloxityOptions veloxityOptions = new VeloxityOptions.Builder(getApplicationContext())
                    .setPriority(< PRIORITY >)
                    .setLicenseKey("<LICENSE_KEY>")
                    .setWebServiceEndpoint("<WEB_SERVICE_URL>")
                    .setFCMProjectID("<FCM_PROJECT_ID>")
                    .setFCMSenderID("<FCM_SENDER_ID>")
                    .setFCMAppID("<FCM_APP_ID>")
                    .setFCMApiKey("<FCM_API_KEY>")
                    .setDialogTitle(getResources().getString(R.string.data_usage_title))
                    .setDialogMessage(getResources().getString(R.string.data_usage_message))
                    .setListener(new DataUsageListener() {
                        @Override
                        public void onDataUsageResult(boolean isServiceStarted) {
                            Log.i("Veloxity",
                                    "Data Usage Page Accepted and Service Started: " + isServiceStarted);
                            if (isServiceStarted) {
                                veloxity_sdk.setChecked(true);
                            } else {
                                veloxity_sdk.setChecked(false);
                            }
                        }

                        @Override
                        public void onCompleted() {
                            Log.i("Veloxity",
                                    "Data Usage Page is finished");
                        }
                    })
                    .build();
            Veloxity.optIn(veloxityOptions);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
