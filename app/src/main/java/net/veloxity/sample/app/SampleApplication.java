package net.veloxity.sample.app;

import net.veloxity.sdk.Veloxity;

import android.app.Application;

public class SampleApplication extends Application {
   @Override
   public void onCreate() {
      super.onCreate();
      if (Veloxity.isInVeloxityProcess(this)) {
         return;
      }
      Veloxity.registerLifeCycleCallbacks(this);
   }
}
