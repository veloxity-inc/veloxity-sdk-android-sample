
package net.veloxity.sample.app;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import net.veloxity.sdk.DataUsageListener;
import net.veloxity.sdk.FCMApiKeyException;
import net.veloxity.sdk.FCMAppIdException;
import net.veloxity.sdk.FCMProjectIdException;
import net.veloxity.sdk.FCMSenderIdException;
import net.veloxity.sdk.LicenseException;
import net.veloxity.sdk.Veloxity;
import net.veloxity.sdk.VeloxityOptions;

import org.json.JSONException;
import org.json.JSONObject;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        try {
            VeloxityOptions veloxityOptions = new VeloxityOptions.Builder(getApplicationContext())
                    .setPriority(<PRIORITY>)
                    .setLicenseKey("<LICENSE_KEY>")
                    .setWebServiceEndpoint("<WEB_SERVICE_URL>")
                    .setFCMProjectID("<FCM_PROJECT_ID>")
                    .setFCMSenderID("<FCM_SENDER_ID>")
                    .setFCMAppID("<FCM_APP_ID>")
                    .setFCMApiKey("<FCM_API_KEY>")
                    .setDialogTitle(getResources().getString(R.string.data_usage_title))
                    .setDialogMessage(getResources().getString(R.string.data_usage_message))
                    .setListener(new DataUsageListener() {
                        @Override
                        public void onDataUsageResult(boolean isServiceStarted) {
                            Log.i("Veloxity",
                                    "Data Usage Page Accepted and Service Started: " + isServiceStarted);
                        }

                        @Override
                        public void onCompleted() {
                            Log.i("Veloxity",
                                    "Data Usage Page is finished");
                        }
                    })
                    .build();
            Veloxity.initialize(veloxityOptions);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_settings:
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
